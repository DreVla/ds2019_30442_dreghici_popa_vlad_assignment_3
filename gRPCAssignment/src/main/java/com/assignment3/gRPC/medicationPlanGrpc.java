package com.assignment3.gRPC;

import javax.annotation.Generated;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: medPlan.proto")
public final class medicationPlanGrpc {

  private medicationPlanGrpc() {}

  public static final String SERVICE_NAME = "medicationPlan";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.assignment3.gRPC.MedPlan.Patient,
      com.assignment3.gRPC.MedPlan.MedicationPlan> getDisplayMedicationPlanMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "displayMedicationPlan",
      requestType = com.assignment3.gRPC.MedPlan.Patient.class,
      responseType = com.assignment3.gRPC.MedPlan.MedicationPlan.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.assignment3.gRPC.MedPlan.Patient,
      com.assignment3.gRPC.MedPlan.MedicationPlan> getDisplayMedicationPlanMethod() {
    io.grpc.MethodDescriptor<com.assignment3.gRPC.MedPlan.Patient, com.assignment3.gRPC.MedPlan.MedicationPlan> getDisplayMedicationPlanMethod;
    if ((getDisplayMedicationPlanMethod = medicationPlanGrpc.getDisplayMedicationPlanMethod) == null) {
      synchronized (medicationPlanGrpc.class) {
        if ((getDisplayMedicationPlanMethod = medicationPlanGrpc.getDisplayMedicationPlanMethod) == null) {
          medicationPlanGrpc.getDisplayMedicationPlanMethod = getDisplayMedicationPlanMethod = 
              io.grpc.MethodDescriptor.<com.assignment3.gRPC.MedPlan.Patient, com.assignment3.gRPC.MedPlan.MedicationPlan>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "medicationPlan", "displayMedicationPlan"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.assignment3.gRPC.MedPlan.Patient.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.assignment3.gRPC.MedPlan.MedicationPlan.getDefaultInstance()))
                  .setSchemaDescriptor(new medicationPlanMethodDescriptorSupplier("displayMedicationPlan"))
                  .build();
          }
        }
     }
     return getDisplayMedicationPlanMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static medicationPlanStub newStub(io.grpc.Channel channel) {
    return new medicationPlanStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static medicationPlanBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new medicationPlanBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static medicationPlanFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new medicationPlanFutureStub(channel);
  }

  /**
   */
  public static abstract class medicationPlanImplBase implements io.grpc.BindableService {

    /**
     */
    public void displayMedicationPlan(com.assignment3.gRPC.MedPlan.Patient request,
        io.grpc.stub.StreamObserver<com.assignment3.gRPC.MedPlan.MedicationPlan> responseObserver) {
      asyncUnimplementedUnaryCall(getDisplayMedicationPlanMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getDisplayMedicationPlanMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.assignment3.gRPC.MedPlan.Patient,
                com.assignment3.gRPC.MedPlan.MedicationPlan>(
                  this, METHODID_DISPLAY_MEDICATION_PLAN)))
          .build();
    }
  }

  /**
   */
  public static final class medicationPlanStub extends io.grpc.stub.AbstractStub<medicationPlanStub> {
    private medicationPlanStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlanStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlanStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationPlanStub(channel, callOptions);
    }

    /**
     */
    public void displayMedicationPlan(com.assignment3.gRPC.MedPlan.Patient request,
        io.grpc.stub.StreamObserver<com.assignment3.gRPC.MedPlan.MedicationPlan> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDisplayMedicationPlanMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class medicationPlanBlockingStub extends io.grpc.stub.AbstractStub<medicationPlanBlockingStub> {
    private medicationPlanBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlanBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlanBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationPlanBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.assignment3.gRPC.MedPlan.MedicationPlan displayMedicationPlan(com.assignment3.gRPC.MedPlan.Patient request) {
      return blockingUnaryCall(
          getChannel(), getDisplayMedicationPlanMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class medicationPlanFutureStub extends io.grpc.stub.AbstractStub<medicationPlanFutureStub> {
    private medicationPlanFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private medicationPlanFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected medicationPlanFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new medicationPlanFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.assignment3.gRPC.MedPlan.MedicationPlan> displayMedicationPlan(
        com.assignment3.gRPC.MedPlan.Patient request) {
      return futureUnaryCall(
          getChannel().newCall(getDisplayMedicationPlanMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_DISPLAY_MEDICATION_PLAN = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final medicationPlanImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(medicationPlanImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_DISPLAY_MEDICATION_PLAN:
          serviceImpl.displayMedicationPlan((com.assignment3.gRPC.MedPlan.Patient) request,
              (io.grpc.stub.StreamObserver<com.assignment3.gRPC.MedPlan.MedicationPlan>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class medicationPlanBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    medicationPlanBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.assignment3.gRPC.MedPlan.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("medicationPlan");
    }
  }

  private static final class medicationPlanFileDescriptorSupplier
      extends medicationPlanBaseDescriptorSupplier {
    medicationPlanFileDescriptorSupplier() {}
  }

  private static final class medicationPlanMethodDescriptorSupplier
      extends medicationPlanBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    medicationPlanMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (medicationPlanGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new medicationPlanFileDescriptorSupplier())
              .addMethod(getDisplayMedicationPlanMethod())
              .build();
        }
      }
    }
    return result;
  }
}
