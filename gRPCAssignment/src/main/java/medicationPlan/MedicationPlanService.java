package medicationPlan;

import com.assignment3.gRPC.MedPlan;
import com.assignment3.gRPC.medicationPlanGrpc;
import io.grpc.stub.StreamObserver;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanService extends medicationPlanGrpc.medicationPlanImplBase {

    @Override
    public void displayMedicationPlan(MedPlan.Patient request, StreamObserver<MedPlan.MedicationPlan> responseObserver) {
        System.out.println("Inside display medication method");
        String patientName = request.getPatientName();
        int patientId = request.getId();

        responseObserver.onNext(loadMedicationPlan().build());
        responseObserver.onCompleted();

    }

    private MedPlan.MedicationPlan.Builder loadMedicationPlan(){
        // here after we receive the user we will have
        // to make a search after id and find the medication plan for him
        // after that, we will have to return the medication plan as
        // response
        List<MedPlan.Medication> medicationList = loadMedication();

        MedPlan.MedicationPlan.Builder medicationPlan = MedPlan.MedicationPlan.newBuilder();
        medicationPlan.setId(1);
        for(MedPlan.Medication m : medicationList){
            medicationPlan.addMedicationList(m);
        }

        return medicationPlan;
    }

    private List<MedPlan.Medication> loadMedication(){
        List<MedPlan.Medication> medicationList = new ArrayList<>();
        MedPlan.Medication.Builder medication = MedPlan.Medication.newBuilder();
        medication.setId(1);
        medication.setName("Algocalmin");
        medication.setIntake("1/day");
        MedPlan.Medication.Builder medication2 = MedPlan.Medication.newBuilder();
        medication2.setId(2);
        medication2.setName("Bromazepam");
        medication2.setIntake("2/day");
        MedPlan.Medication.Builder medication3 = MedPlan.Medication.newBuilder();
        medication3.setId(3);
        medication3.setName("Strepsils");
        medication3.setIntake("3/day");
        medicationList.add(medication.build());
        medicationList.add(medication2.build());
        medicationList.add(medication3.build());
        return medicationList;
    }
}
