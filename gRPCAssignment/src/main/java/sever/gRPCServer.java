package sever;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import medicationPlan.MedicationPlanService;

import java.io.IOException;

public class gRPCServer {

    public static void main(String[] args) throws IOException, InterruptedException {

        Server server = ServerBuilder.forPort(9090).addService(new MedicationPlanService()).build();
        server.start();
        System.out.println("Server started at " + server.getPort());

        server.awaitTermination();
    }
}
