import com.assignment3.gRPC.MedPlan;
import com.assignment3.gRPC.medicationPlanGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;

import java.text.DateFormat;
import java.util.*;

public class gRPCClient extends Application{


    private static List<String> medicationNames = new ArrayList<>();
    static ManagedChannel channel;

    static Timer timer = new Timer();
    public static void main(String[] args){

        channel = ManagedChannelBuilder.forAddress("localhost", 9090).usePlaintext().build();


        MedPlan.MedicationPlan medicationPlan = refreshMedicationPlan();
        List<MedPlan.Medication> medications = new ArrayList<>();
        medications.addAll(medicationPlan.getMedicationListList());
        System.out.println("Patient" + "\n");
        System.out.println("Medication Plan: \n");
        System.out.println(medicationPlan.getId() + "\n");
        for(MedPlan.Medication m : medications){
            medicationNames.add(m.getName() + " " + m.getIntake());
            System.out.println(m.getName() + " " + m.getIntake());
        }
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Your database code here
                MedPlan.MedicationPlan medicationPlan = refreshMedicationPlan();
                List<MedPlan.Medication> medications = new ArrayList<>();
                medications.addAll(medicationPlan.getMedicationListList());
                medicationNames.clear();
                for(MedPlan.Medication m : medications){
                    medicationNames.add(m.getName() + " " + m.getIntake());
                    System.out.println(m.getName() + " " + m.getIntake());
                }
            }
        }, 1000, 1000);

        launch();
    }

    @Override
    public void start(Stage stage) {
//        String javaVersion = System.getProperty("java.version");
//        String javafxVersion = System.getProperty("javafx.version");
//        Label l = new Label("Hello, JavaFX " + javafxVersion + ", running on Java " + javaVersion + ".");
        StackPane rootPane = new StackPane();
        Scene scene = new Scene(rootPane, 480, 640);
        stage.setScene(scene);

        Pane pane1 = new Pane();
        Pane pane2 = new Pane();


        // Timer
        final Label clock = new Label();
        final DateFormat format = DateFormat.getInstance();
        final Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                final Calendar cal = Calendar.getInstance();
                clock.setText(format.format(cal.getTime()));
            }
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        pane2.setPrefHeight(48);
        pane2.setPrefWidth(48);
        pane2.getChildren().add(clock);

        // Medication Plan
        ListView<String> list = new ListView<String>();
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll(medicationNames);
        ListView<String> lv = new ListView<String>(items);
        lv.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new XCell();
            }
        });

        lv.relocate(0,48);
        lv.setPrefHeight(640);
        lv.setPrefWidth(480);
        pane1.getChildren().add(lv);

        rootPane.getChildren().addAll(pane2,pane1);
        stage.show();
    }

    static class XCell extends ListCell<String> {
        HBox hbox = new HBox();
        Label label = new Label("(empty)");
        Pane pane = new Pane();
        Button button = new Button("Take");
        String lastItem;

        public XCell() {
            super();
            hbox.getChildren().addAll(label, pane, button);
            HBox.setHgrow(pane, Priority.ALWAYS);
            button.setText("Take");
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    System.out.println(lastItem + " : " + actionEvent);
                    button.setText("Medication Taken");
                }
            });
        }

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);  // No text in label of super class
            if (empty) {
                lastItem = null;
                setGraphic(null);
            } else {
                lastItem = item;
                label.setText(item!=null ? item : "<null>");
                setGraphic(hbox);
            }
        }
    }

    public static MedPlan.MedicationPlan refreshMedicationPlan(){
        // stubs generate from protofiles
        medicationPlanGrpc.medicationPlanBlockingStub medPlanStub = medicationPlanGrpc.newBlockingStub(channel);

        MedPlan.Patient patient = MedPlan.Patient.newBuilder().setId(1).setPatientName("Patient").build();
        MedPlan.MedicationPlan medicationPlan = medPlanStub.displayMedicationPlan(patient);
        return medicationPlan;
    }

}
